"""intial

Revision ID: e0267271b95f
Revises: 
Create Date: 2021-05-23 16:16:27.743855+01:00

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'e0267271b95f'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table('tax_rules',
        sa.Column('left_bound', sa.Numeric(12, 2), nullable=True),
        sa.Column('percent', sa.SmallInteger(), nullable=True),
        sa.CheckConstraint('left_bound >= 0', name='left_bound_positive_check'),
        sa.CheckConstraint('percent >= 0 AND percent <= 100', name='percent_range_check')
    )


def downgrade():
    op.drop_table('tax_rules')
