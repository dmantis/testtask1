"""basic data for convenience

Revision ID: d12fbf76d863
Revises: e0267271b95f
Create Date: 2021-05-23 17:25:13.116265+01:00

"""
import sqlalchemy as sa

from alembic import op

# revision identifiers, used by Alembic.
revision = 'd12fbf76d863'
down_revision = 'e0267271b95f'
branch_labels = None
depends_on = None


_table = sa.table(
    'tax_rules',
    sa.column('left_bound', sa.Numeric),
    sa.column('percent', sa.Integer)
)


_basic_data = [
    {'left_bound': 0, 'percent': 0},
    {'left_bound': 12500, 'percent': 20},
    {'left_bound': 50000, 'percent': 40},
    {'left_bound': 150000, 'percent': 45},
]


def upgrade():
    op.bulk_insert(_table, _basic_data)


def downgrade():
    pass
