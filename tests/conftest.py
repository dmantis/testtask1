import os

import pytest
from async_asgi_testclient import TestClient

import alembic.command
import alembic.config
from src import app, config, controller


@pytest.fixture()
def patched_get_config(mocker):
    test_config = config.Config(
        DB_DSN=f'sqlite:////tmp/sqlite_{os.urandom(16).hex()}.db',
    )
    mock = mocker.patch('src.config.get_config', return_value=test_config)
    yield mock


@pytest.fixture()
def patched_get_db(mocker, patched_get_config):
    alembic_config = alembic.config.Config('alembic.ini')
    conf = config.get_config()
    alembic_config.set_main_option('sqlalchemy.url', conf.DB_DSN)
    alembic_config.set_main_option('script_location', 'alembic')
    alembic.command.upgrade(alembic_config, 'head')
    _controller = controller.Controller(conf.DB_DSN)
    mock = mocker.patch('src.controller.get_db', return_value=_controller)
    yield mock


@pytest.fixture()
async def client(patched_get_db):
    async with TestClient(app.create_app()) as client:
        yield client
