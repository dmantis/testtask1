import pytest


TAX_ENDPOINT = '/tax'


@pytest.mark.asyncio
async def test_tax(client):
    resp = await client.get(TAX_ENDPOINT, query_string={'amount': '13500'})
    assert resp.status_code == 200
    data = resp.json()
    assert data == {'total': '200.00'}


@pytest.mark.asyncio
async def test_tax_detailed(client):
    resp = await client.get(
        TAX_ENDPOINT, query_string={'amount': '60000', 'detailed': True})
    assert resp.status_code == 200
    data = resp.json()
    assert data == {
        'total': '11500.00',
        'detailed': [
            {'bound': '50000.00', 'tax_amount': '4000.00'},
            {'bound': '12500.00', 'tax_amount': '7500.00'},
            {'bound': '0.00', 'tax_amount': '0.00'},
        ]
    }


@pytest.mark.asyncio
async def test_tax_no_params(client):
    resp = await client.get(TAX_ENDPOINT)
    assert resp.status_code == 422


@pytest.mark.asyncio
async def test_tax_bad_range(client):
    resp = await client.get(TAX_ENDPOINT, query_string={'amount': '-100'})
    assert resp.status_code == 422
