""" Database models. """
from decimal import Decimal

import pydantic
import sqlalchemy as sa

metadata = sa.MetaData()

# I assume that tax percents are integers and there is no taxes such as 25.5%.
#  If the task requires to handle decimal percents - just send me a message,
#  I'll modify that part.

tax_rules = sa.Table(
    'tax_rules',
    metadata,
    sa.Column('left_bound', sa.Numeric()),
    sa.Column('percent', sa.SmallInteger()),
    # check constraints
    sa.CheckConstraint('left_bound >= 0', 'left_bound_positive_check'),
    sa.CheckConstraint('percent >= 0 AND percent <= 100',
                       'percent_range_check'),
)


class TaxRuleDB(pydantic.BaseModel):
    left_bound: Decimal
    percent: int

    class Config:
        orm_mode = True
