""" Input/Output schemas. """
from decimal import ROUND_UP, Decimal
from typing import List, Optional

import pydantic


def decimal_encoder(x: Decimal):
    return str(x.quantize(Decimal('1.00'), rounding=ROUND_UP))


class DecimalStrEncoder(pydantic.BaseModel):
    """ To pass money as strings instead of floats due to possible rounding
    mistakes. """

    class Config:
        json_encoders = {Decimal: decimal_encoder}


class TaxStep(DecimalStrEncoder):
    bound: Decimal
    tax_amount: Decimal


class TaxOut(DecimalStrEncoder):
    total: Decimal
    detailed: Optional[List[TaxStep]] = None
