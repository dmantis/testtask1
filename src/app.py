from fastapi import FastAPI

from .routers import main


def create_app() -> FastAPI:
    app = FastAPI()
    app.include_router(main.router, prefix='')
    return app
