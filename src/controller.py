import functools
from decimal import Decimal
from typing import List

import databases

from . import config, models


class Controller(databases.Database):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    async def get_tax_rules_for_amount(
            self, value: Decimal) -> List[models.TaxRuleDB]:
        query = models.tax_rules.select().where(
            models.tax_rules.c.left_bound < value
        ).order_by(
            models.tax_rules.c.left_bound,
        )
        rows = await self.fetch_all(query)
        return [models.TaxRuleDB.from_orm(row) for row in rows]


@functools.lru_cache()
def get_db():
    conf = config.get_config()
    controller = Controller(conf.DB_DSN)
    return controller
