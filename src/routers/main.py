from decimal import Decimal
from typing import Dict

import fastapi

from .. import controller, schemas

router = fastapi.APIRouter()
dp_dep = fastapi.Depends(controller.get_db)


@router.on_event('startup')
async def startup_event():
    db = controller.get_db()
    await db.connect()


@router.on_event('shutdown')
async def shutdown_event():
    db = controller.get_db()
    if db.is_connected:
        await db.disconnect()


@router.get('/tax',
            response_model=schemas.TaxOut,
            response_model_exclude_none=True)
async def tax(
        amount: Decimal = fastapi.Query(..., ge=0),
        detailed: bool = False,
        db: controller.Controller = dp_dep,
):
    tax_rules = await db.get_tax_rules_for_amount(amount)
    taxes: Dict[Decimal, Decimal] = {}
    for tax_rule in reversed(tax_rules):
        taxed_amount = amount - tax_rule.left_bound
        taxes[tax_rule.left_bound] = taxed_amount * tax_rule.percent / 100
        amount = tax_rule.left_bound
    total_tax = sum(taxes.values())
    response = schemas.TaxOut(total=total_tax)
    if detailed:
        response.detailed = [schemas.TaxStep(bound=k, tax_amount=v)
                             for k, v in taxes.items()]
    return response
