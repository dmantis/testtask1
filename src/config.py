import functools

import pydantic


class Config(pydantic.BaseSettings):
    DB_DSN: str = 'sqlite:///sqlite.db'


@functools.lru_cache(maxsize=1)
def get_config():
    return Config()
