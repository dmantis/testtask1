# Test task

## Task description

In the UK, the amount of income tax one has to pay is calculated through a number 
of tax bands. For the current year, it sits as follows:
    
* 0% on earnings up to £12,500
* 20% on any earnings between £12,501 and £50,000
* 40% on any earnings between £50,001 and £150,000
* 45% on earnings of £150,001 and over

For example, if you earn £52,000 a year, you pay 

* 0 on the first £12,500
* 20% on the £37,500 (£12,501 - £50,000) -> £7,500
* 40% on the remaining £2,000  -> £800

In total, you would pay £8,300 income tax. 

Please write a small web service in Python with one single REST endpoint (using Django, 
Flaks or a framework/server of your choice) that will calculate how much income 
tax one would pay. It will receive as input, the annual salary; and provide as 
output, the total income tax payable for the year. If a flag for detailed output 
is set, then the service will return, in addition to the total income tax payable, 
the tax payable at each band. 

**Please demonstrate some use of OOP/python classes in your code.**

The tax bands should be loaded from an external source, such as a file, 
an http URL, a cloud storage bucket, or any kind of database, such as SQLite, 
MySQL, Redis, MongoDB, etc. 

## Solution

The solution uses FastAPI as web framework, sqlalchemy as ORM and
sqlite as database.

Poetry is used for dependencies management. docker/docker-compose are in use for
fast building and deployment.

With default settings, endpoint is available by GET http://127.0.0.1:8080/tax request.

## Deployment (Docker)

1. Run migrations

```bash
touch /tmp/sqlite_testtask.db
docker-compose run web alembic upgrade head
```

If you wish to run tests, run:

```bash
docker-compose run web pytest
```

2. Finally, run the service
```bash
docker-compose up -d
```

## Deployment (Without docker)

Set DB_DSN env variable, install dependencies, run migrations and run uvicorn 
asgi server:
```bash
export DB_DSN=sqlite///sqlite.db
poetry install
poetry shell
alembic upgrade head
python -m uvicorn src.main:app --host 127.0.0.1 --port 8080
```