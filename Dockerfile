FROM python:3.9

COPY ./pyproject.toml ./poetry.lock ./
RUN pip install poetry && \
    poetry export -f requirements.txt --output requirements.txt  --dev && \
    pip install -r requirements.txt

COPY . /app/
WORKDIR /app

ENV HOST 0.0.0.0
ENV PORT 8080
ENV LOG_LEVEL info

CMD python -m uvicorn src.main:app --host $HOST --port $PORT --log-level $LOG_LEVEL
